with Ada.Text_IO;
with Ada.Containers.Vectors;

procedure Main is
   Count_Wave : Integer := 1;
   Count_Number : Integer := 100;
   Size : Float := Float(Count_Number);
   Coun : Integer := 0;

   package Data_Vectors is new Ada.Containers.Vectors (Index_Type => Natural, Element_Type => Integer);
   use Data_Vectors;
   Vector_Data : Data_Vectors.Vector;

   task type Task_Wave is
      entry Start;
   end Task_Wave;

   type Task_Ptr is access all Task_Wave;
   type Task_Array is array (1..Count_Number) of Task_Ptr;
   Tasks : Task_Array;

   task body Task_Wave is
      function Sum (Num1, Num2 : Integer) return Integer is
      begin
         return Num1 + Num2;
      end Sum;
      Result : Data_Vectors.Vector;
   begin
      accept Start do
         Ada.Text_IO.Put("Initial array: [");
         for Elem of Vector_Data loop
            Ada.Text_IO.Put(Integer'Image(Elem));
            if Elem /= Vector_Data.Last_Element then
               Ada.Text_IO.Put(", ");
            else
               Ada.Text_IO.Put_Line("].");
            end if;
         end loop;

         Ada.Text_IO.Put("Wave" & Integer'Image(Count_Wave) & ":");
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("pairs - ");
         for I in 1 .. (Integer(Vector_Data.Length) + 1) / 2 loop
            if Integer(Vector_Data.Length) = 1 then
               Result.Append(Vector_Data.First_Element);
               Ada.Text_IO.Put(Integer'Image(Vector_Data.First_Element));
            else
               declare
                  Elem1 : Integer := Vector_Data.First_Element;
                  Elem2 : Integer := Vector_Data.Last_Element;
               begin
                  Ada.Text_IO.Put(Integer'Image(Elem1) & " + " & Integer'Image(Elem2));
                  Result.Append(Sum(Elem1, Elem2));
               end;
               Vector_Data.Delete_First;
               Vector_Data.Delete_Last;
            end if;

            if I /= (Integer(Vector_Data.Length) + 1) / 2 then
               Ada.Text_IO.Put("; ");
            else
               Ada.Text_IO.New_Line;
            end if;
         end loop;

         Ada.Text_IO.Put("Result - [");
         for Elem of Result loop
            Ada.Text_IO.Put(Integer'Image(Elem));
            if Elem /= Result.Last_Element then
               Ada.Text_IO.Put(", ");
            else
               Ada.Text_IO.Put("] - actual part, {");
               for Item of Vector_Data loop
                  Ada.Text_IO.Put(Integer'Image(Item));
                  if Item /= Vector_Data.Last_Element then
                     Ada.Text_IO.Put(", ");
                  else
                     Ada.Text_IO.Put_Line("} - used items.");
                  end if;
               end loop;
            end if;
         end loop;

         Ada.Text_IO.New_Line;

         Vector_Data := Result;
         Result.Clear;
         Count_Wave := Count_Wave + 1;  -- Increment Count_Wave
      end Start;
   end Task_Wave;

begin
   for I in 1 .. Count_Number loop
      Vector_Data.Append(I);
   end loop;

   for I in Tasks'Range loop
      Tasks(I) := new Task_Wave;
      Tasks(I).Start;
   end loop;

   while Size >= 1.0 loop
      Size := Size / 2.0;
      Coun := Coun + 1;
   end loop;
end Main;
