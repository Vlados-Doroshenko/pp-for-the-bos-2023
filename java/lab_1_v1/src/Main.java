import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {
        MultiThreadCalculator calculator = new MultiThreadCalculator();
        calculator.start();
    }
}

class MultiThreadCalculator {
    private int arraySize;
    private int[] calculateArray;
    private ExecutorService executorService;
    private BlockingQueue<Integer> indexQueue;

    public void start() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input an array size: ");
        arraySize = input.nextInt();
        calculateArray = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            calculateArray[i] = i + 1;
        }

        System.out.println("Initial array: " + arrayToString(calculateArray));

        int numThreads = Runtime.getRuntime().availableProcessors();
        executorService = new ThreadPoolExecutor(numThreads, numThreads,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        indexQueue = new LinkedBlockingQueue<>();

        int wave = 1;

        while (arraySize > 1) {
            System.out.println("Wave " + wave + ":");
            calculateWave();
            arraySize = (arraySize + 1) / 2;
            System.out.println("Result - " + arrayToString(0, arraySize) + " - actual part, " +
                    arrayToString(arraySize, calculateArray.length) + " - \"used\" items.");
            wave++;
        }

        System.out.println(calculateArray[0] + " - sum of array elements.");

        executorService.shutdown();

        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void calculateWave() {
        for (int i = 0; i < arraySize / 2; i++) {
            indexQueue.offer(i);
        }

        CountDownLatch latch = new CountDownLatch(arraySize / 2);

        for (int i = 0; i < ((ThreadPoolExecutor) executorService).getMaximumPoolSize(); i++) {
            executorService.execute(() -> {
                while (!indexQueue.isEmpty()) {
                    try {
                        int index = indexQueue.take();
                        int symmetricIndex = arraySize - 1 - index;
                        calculateArray[index] += calculateArray[symmetricIndex];
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                latch.countDown();
            });
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String arrayToString(int startIndex, int endIndex) {
        int[] subArray = Arrays.copyOfRange(calculateArray, startIndex, endIndex);
        return Arrays.toString(subArray);
    }

    private String arrayToString(int[] arr) {
        return Arrays.toString(arr);
    }
}
