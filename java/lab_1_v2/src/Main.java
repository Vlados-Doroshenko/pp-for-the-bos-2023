import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class Main {
    public static void main(String[] args) {
        MultiThreadCalculator calculator = new MultiThreadCalculator();
        calculator.start();
    }
}

class MultiThreadCalculator {
    private int arraySize;
    private int[] calculateArray;

    public void start() {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Input an array size: ");
            arraySize = input.nextInt();
            calculateArray = new int[arraySize];
            for (int i = 0; i < arraySize; i++) {
                calculateArray[i] = i + 1;
            }

            System.out.println("Initial array: " + arrayToString(calculateArray));

            int wave = 1;

            while (arraySize > 1) {
                System.out.println("Wave " + wave + ":");
                calculateWave();
                arraySize = (arraySize + 1) / 2;
                System.out.println("Result - " + arrayToString(0, arraySize) + " - actual part, " +
                        arrayToString(arraySize, calculateArray.length) + " - \"used\" items.");
                wave++;
            }

            System.out.println(calculateArray[0] + " - sum of array elements.");
        }
    }

    private void calculateWave() {
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.invoke(new WaveTask(0, arraySize / 2));
        forkJoinPool.shutdown();
    }

    private String arrayToString(int startIndex, int endIndex) {
        int[] subArray = Arrays.copyOfRange(calculateArray, startIndex, endIndex);
        return Arrays.toString(subArray);
    }

    private String arrayToString(int[] arr) {
        return Arrays.toString(arr);
    }

    private class WaveTask extends RecursiveAction {
        private static final int THRESHOLD = 100;

        private final int startIndex;
        private final int endIndex;

        private WaveTask(int startIndex, int endIndex) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }

        @Override
        protected void compute() {
            if (endIndex - startIndex <= THRESHOLD) {
                for (int i = startIndex; i < endIndex; i++) {
                    int symmetricIndex = arraySize - 1 - i;
                    calculateArray[i] += calculateArray[symmetricIndex];
                }
            } else {
                int middle = (startIndex + endIndex) / 2;
                invokeAll(new WaveTask(startIndex, middle), new WaveTask(middle, endIndex));
            }
        }
    }
}
